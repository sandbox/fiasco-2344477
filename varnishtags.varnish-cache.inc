<?php
/**
 * @file
 * Database cache integration.
 */

require_once dirname(__FILE__) . '/../varnish/varnish.cache.inc';

class VarnishTagsCache extends VarnishCache {
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $headers = varnishtags_headers();
    array_map('drupal_add_http_header', array_keys($headers), array_values($headers));
  }
}
