<?php

/**
 * @file
 * Cache include file, to be used in settings.php file.
 */

/**
 * Varnish cache implementation.
 */
class VarnishTagsCache implements DrupalCacheInterface {
  protected $bin;

  function __construct($bin) {
    $this->bin = $bin;
  }

  function get($cid) {
    return FALSE;
  }

  function getMultiple(&$cids) {
    return array();
  }

  function set($cid, $data, $expire = CACHE_PERMANENT) {
    $headers = varnishtags_headers();
    array_map('drupal_add_http_header', array_keys($headers), array_values($headers));
  }

  function clear($cid = NULL, $wildcard = FALSE) {
  }

  function isEmpty() {
    return FALSE;
  }
}
