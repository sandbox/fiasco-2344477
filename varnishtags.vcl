# Example VCL you may want to use for HTTP style banning.
sub vcl_recv {
  if (req.request == "DELETE" && req.http.X-VarnishTags-Ban-Key) {

    # Should also do something like this to prevent unauthorized bans
    # if (client.ip ~ ban_acl) {
    #   error 405 "Not allowed";
    # }

    ban("req.http." + req.http.X-VarnishTags-Ban-Key + " ~ ," + req.http.X-VarnishTags-Ban-Value + ",");
    error 200 "Banned " + req.http.X-VarnishTags-Ban-Key + ":" + req.http.X-VarnishTags-Ban-Value;
  }
}
